import Vue from "vue";
import VueRouter from "vue-router";
import searchMovies from "../views/searchMovies.vue";
import categories from "../views/categories.vue";
import favorites from "../views/favorites";
import config from "../../config";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        redirect: '/searchMovies',
    },
    {
        path: "/searchMovies",
        name: "searchMovies",
        component: searchMovies
    },
    {
        path: "/categories",
        name: "categories",
        component: categories
    },
    {
        path: "/favorites",
        name: "favorites",
        component: favorites
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    if(!routes.some(item => item.path === to.path)){
        next({path: '/'});
        return;
    }
    if(to.name === "categories"){
        if(config.CATEGORIES.some(item => +item.index === +to.query.id)){
            next();
            return;
        } else {
            to.query.id = 2;
            next();
            return;
        }
    }
    next();
});

export default router;
