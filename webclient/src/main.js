import Vue from "vue";
import App from "./App.vue";
import router from "./router";
//bootstrap
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
//CSS
import "./assets/global_css.css";
//Fontawesome
import {library} from '@fortawesome/fontawesome-svg-core'
import {faPlay, faStar} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
//Axios
import axios from "axios";
import VueAxios from "vue-axios";
//Dialog
import VuejsDialog from "vuejs-dialog";
import "vuejs-dialog/dist/vuejs-dialog.min.css";
//Snack
import VueSnackbar from "vue-snack";
import "vue-snack/dist/vue-snack.min.css";
//i18n
import VueI18n from "vue-i18n";
//config
import config from "../config";
//VueObserveVisibility
import VueObserveVisibility from 'vue-observe-visibility';
//bootstrap-vue
import {EmbedPlugin, ModalPlugin} from 'bootstrap-vue';

library.add(faPlay)
library.add(faStar);
Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.use(VueAxios, axios);

Vue.use(VuejsDialog);

Vue.use(VueSnackbar, {position: "bottom", time: 3000});

Vue.use(VueI18n);

Vue.use(VueObserveVisibility);

Vue.use(EmbedPlugin);
Vue.use(ModalPlugin);

Vue.config.productionTip = false;

const i18n = new VueI18n({
    locale: 'en-US',
    messages: {
        'en-US': {
            /*Columns*/
            pictureText: "Picture",
            titleText: "Title",
            descriptionText: "Description",
            releaseDateText: "Release date",
            ratingText: "Rating",
            trailerText: "Trailer",
            /*Navigation*/
            searchMoviesText: "Search for movies",
            latest: "Latest movie",
            now_playing: "Now playing",
            popular: "Popular",
            top_rated: "Top rated",
            upcoming: "Upcoming",
            favoritesText: "Your favorites",
            /***/
            noMoviesFound: "No movies found....",
            searchForMovie: "Search for a movie",
            searchText: "Search",
            clearText: "Clear",
            altImageText: "Movie image",
            backText: "Back",
            noTrailerText: "No trailer found!",
            invalidSearchInput: "Please input a valid value for searching.",
            moviesCountText: " movies found.",
            noFavoritesFoundText: "You have no favorites.",
            /*Error*/
            categoryError: "Request failed! This category is invalid.",
            trailerError: "Trailer could not be loaded!",
            favoritesError: "Favorites could not be loaded!",
        },
        'de-DE': {
            /*Columns*/
            pictureText: "Bild",
            titleText: "Titel",
            descriptionText: "Beschreibung",
            releaseDateText: "Erscheinungsdatum",
            ratingText: "Bewertung",
            trailerText: "Trailer",
            /*Navigation*/
            searchMoviesText: "Filme suchen",
            latest: "Neuester Film",
            now_playing: "Jetzt im Kino",
            popular: "Beliebt",
            top_rated: "Bestbewertet",
            upcoming: "Kommende Filme",
            favoritesText: "Deine Favoriten",
            /***/
            noMoviesFound: "Keine Filme vorhanden....",
            searchForMovie: "Suche Film",
            searchText: "Suchen",
            clearText: "Zurücksetzen",
            altImageText: "Film Bild",
            backText: "Zurück",
            noTrailerText: "Kein Trailer gefunden!",
            invalidSearchInput: "Bitte geben Sie einen gültigen Suchparameter an.",
            moviesCountText: " Filme gefunden.",
            noFavoritesFoundText: "Sie haben keine Favoriten gespeichert.",
            /*Error*/
            categoryError: "Abfrage fehlgeschlagen! Diese Kategorie ist nicht gültig.",
            trailerError: "Trailer konnte nicht geladen werden!",
            favoritesError: "Favoriten konnten nicht geladen werden!",
        }
    }
});

axios.interceptors.request.use(axiosConfig => {
    axiosConfig.headers.Authorization = `Bearer ${config.API_TOKENV4}`;
    config.params = {...config.params, language: i18n.locale}
    return axiosConfig;
}, error => {
    return Promise.reject(error);
});

new Vue({
    router,
    render: h => h(App),
    i18n
}).$mount("#app");
