export default class config {
    static get API_TOKENV3() {
        return "e241d3133a3e094478ded92566902531";
    }

    static get API_TOKENV4() {
        return "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJlMjQxZDMxMzNhM2UwOTQ0NzhkZWQ5MjU2NjkwMjUzMSIsInN1YiI6IjVlYTk1MDQ2YjdmYmJkMDAxZDNhMmNjOCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.zCWDRaC1sl-IU7iZ7o6W1WHimNpcAW1CEUOtPAUTlTM";
    }

    static get SEARCHURL() {
        return "https://api.themoviedb.org/3/search/movie";
    }

    static get BASEURL() {
        return "https://api.themoviedb.org/3/movie/";
    }

    static get CATEGORIES() {
        //name of category has to match with the API given Category URL name
        return [{name: "latest", index: 1}, {name: "now_playing", index: 2}, {name: "popular", index: 3}, {name: "top_rated", index: 4}, {name: "upcoming", index: 5}];
    }
}